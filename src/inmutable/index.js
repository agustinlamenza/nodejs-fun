function personFactory(name = '', lastName = '', age = 0, date = Date.now()) {
  const state = {
    name,
    lastName,
    age,
    date,
    getDescription() {
      return `${state.name} - ${state.lastName} - ${state.age} - ${state.date}`;
    }
  };

  return Object.freeze(state);
}

const p = personFactory('Agustin', 'Gonzalez Lamenza', 29, new Date(1989, 7, 1));

console.log(p.getDescription());

function carFactory({ model, brand, year }) {
  const state = {
    model,
    brand,
    year,
    getDescription() {
      return `${state.brand} - ${state.model} - ${state.year}`;
    }
  };

  return Object.freeze(state);
}

const c = carFactory({ model: 'X', brand: 'Ford', year: 2022 });

console.log(c.getDescription());
