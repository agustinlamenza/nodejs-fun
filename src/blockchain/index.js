const crypto = require('crypto');

function blockFactory(prevHash = '', fromAddress = '', toAddress = '', amount = 0) {
  const state = {
    prevHash,
    fromAddress,
    toAddress,
    amount,
    hash: '',
    count: 0,
    calculateHash() {
      return crypto
        .createHash('sha256')
        .update(
          `${state.prevHash}-${state.fromAddress}-${state.toAddress}-${state.amount}-${state.count}`
        )
        .digest('hex');
    },
    mineBlock(difficulty) {
      while (state.hash.substring(0, difficulty) !== Array(difficulty + 1).join('0')) {
        state.count += 1;
        state.hash = state.calculateHash();
      }
    }
  };

  state.hash = state.calculateHash();

  return state;
}

function blockChainFactory(difficulty) {
  const state = {
    blockChain: [],
    addBlock(block) {
      block.mineBlock(difficulty);
      state.blockChain.push(block);
    },
    getLastBlock() {
      return state.blockChain[state.blockChain.length - 1];
    },
    isValid() {
      for (let i = 1; i < state.blockChain.length; i += 1) {
        const current = state.blockChain[i];
        if (current.hash !== current.calculateHash()) {
          return false;
        }
      }
      return true;
    }
  };

  // Populate the genesis block
  state.blockChain.push(blockFactory());

  return state;
}

const bc = blockChainFactory(5);

bc.addBlock(blockFactory(bc.getLastBlock().hash, 'address1', 'address2', 10));

bc.addBlock(blockFactory(bc.getLastBlock().hash, 'address2', 'address1', 5));

// bc.blockChain[1].amount = 1;

console.log(bc);

console.log(bc.isValid());
