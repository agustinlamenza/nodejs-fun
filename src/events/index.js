const EventEmitter = require('events');

class Logger extends EventEmitter {}

const logger = new Logger();

logger.on('my-event', data => {
  console.log('My event 1:', data);
});

logger.on('my-event', data => {
  console.log('My event 2:', data);
});

logger.on('my-event', data => {
  console.log('My event 3:', data);
});

logger.emit('my-event', 'this is the data');
